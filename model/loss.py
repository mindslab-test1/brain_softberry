import torch


def masked_loss(y_pred, y_true):
    mask_true = (y_true != 0).float()
    mask = (y_pred != 0).float()
    mask_true /= mask_true.mean()
    mask /= mask.mean()
    mae = torch.abs(y_pred - y_true)
    mse = torch.square(y_pred - y_true)
    loss = mae + 0.1 * mse * mask_true
    # trick for nans: https://discuss.pytorch.org/t/how-to-set-nan-in-tensor-to-0/3918/3
    loss[loss != loss] = 0
    return loss.mean()
