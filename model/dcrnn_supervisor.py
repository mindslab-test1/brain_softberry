import sys
import os
import time

import numpy as np
import torch
import io
import matplotlib
matplotlib.use('Agg')
import matplotlib.pylab as plt
import PIL.Image
from torchvision.transforms import ToTensor
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm
import random

from lib import utils
from model.dcrnn_model import DCRNNModel
from model.loss import masked_loss

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class DCRNNSupervisor:
    def __init__(self, adj_mx, chkpt_path, infer=False, **kwargs):
        self._kwargs = kwargs
        self.infer = infer
        self._data_kwargs = kwargs.get('data')
        self._model_kwargs = kwargs.get('model')
        self._train_kwargs = kwargs.get('train')

        self.max_grad_norm = self._train_kwargs.get('max_grad_norm', 1.)

        # logging.
        self._log_dir = self._get_dir(kwargs, infer=infer)
        self._chkpt_dir = self._get_dir(kwargs, chkpt=True, infer=infer)
        self._writer = SummaryWriter(self._log_dir)

        log_level = self._kwargs.get('log_level', 'INFO')
        if infer==False:
            self._logger = utils.get_logger(self._log_dir, __name__, 'info.log', level=log_level)

        # data set
        self._data = utils.load_dataset(**self._data_kwargs)
        mean_std = np.load(os.path.join(self._data_kwargs.get('dataset_dir'), 'mean_std.npz'))
        mean, std = round(float(mean_std['mean']),3), round(float(mean_std['std']),3)
        self.standard_scaler = utils.StandardScaler(mean, std)

        self.num_nodes = int(self._model_kwargs.get('num_nodes', 1))
        self.input_dim = int(self._model_kwargs.get('input_dim', 1))
        self.seq_len = int(self._model_kwargs.get('seq_len'))  # for the encoder
        self.output_dim = int(self._model_kwargs.get('output_dim', 1))
        self.use_curriculum_learning = bool(
            self._model_kwargs.get('use_curriculum_learning', False))
        self.horizon = int(self._model_kwargs.get('horizon', 1))  # for the decoder

        # setup model
        dcrnn_model = DCRNNModel(adj_mx, **self._model_kwargs)
        self.dcrnn_model = dcrnn_model.cuda() if torch.cuda.is_available() else dcrnn_model
        self.optimizer = torch.optim.Adam(self.dcrnn_model.parameters(), lr=self._train_kwargs.get('base_lr'))
        self.lr_scheduler = torch.optim.lr_scheduler.MultiStepLR(self.optimizer, milestones=self._train_kwargs.get('steps'),
                                                                gamma=self._train_kwargs.get('lr_decay_ratio'))
        if infer==False:
            self._logger.info("Model created")

        self.chkpt_path = chkpt_path
        if chkpt_path is not None:
            self.batches_seen, self._epoch_num = self.load_model()
        else:
            self.batches_seen = 0
            self._epoch_num = self._train_kwargs.get('epoch', 0)

    @staticmethod
    def _get_dir(kwargs, chkpt=False, infer=False):
        base_dir = kwargs['train'].get('log_dir')
        batch_size = kwargs['data'].get('batch_size')
        learning_rate = kwargs['train'].get('base_lr')
        max_diffusion_step = kwargs['model'].get('max_diffusion_step')
        num_rnn_layers = kwargs['model'].get('num_rnn_layers')
        rnn_units = kwargs['model'].get('rnn_units')
        structure = '-'.join(
            ['%d' % rnn_units for _ in range(num_rnn_layers)])
        horizon = kwargs['model'].get('horizon')
        filter_type = kwargs['model'].get('filter_type')
        filter_type_abbr = 'L'
        if filter_type == 'random_walk':
            filter_type_abbr = 'R'
        elif filter_type == 'dual_random_walk':
            filter_type_abbr = 'DR'
        run_id = 'dcrnn_50110_minutesofcharging_%s_%d_h_%d_%s_lr_%g_bs_%d_%s/' % (
            filter_type_abbr, max_diffusion_step, horizon,
            structure, learning_rate, batch_size,
            time.strftime('%m%d%H%M%S'))
        if not os.path.exists('models_345/'):
            os.makedirs('models_345/')
        if chkpt:
            dir = os.path.join('models_345/', run_id)
        else:
            dir = os.path.join(base_dir, run_id)
        if not os.path.exists(dir) and infer == False:
            os.makedirs(dir)
        if infer == True and chkpt==False:
            dir = os.path.join(base_dir, 'dcrnn_inference')
        return dir

    def save_model(self, batches_seen, epoch, lr_scheduler):
        config = dict(self._kwargs)
        config['model_state_dict'] = self.dcrnn_model.state_dict()
        #config['optimizer'] = optimizer.state_dict()
        config['scheduler'] = lr_scheduler.state_dict()
        config['batches_seen'] = batches_seen
        config['epoch'] = epoch
        chkpt_path = os.path.join(self._chkpt_dir, 'epo%d.tar' % epoch)
        torch.save(config, chkpt_path)
        self._logger.info("Saved model at {}".format(epoch))
        return chkpt_path

    def load_model(self):
        self._setup_graph()
        chkpt_path = self.chkpt_path
        assert os.path.exists(chkpt_path), 'Checkpoint is not found'
        checkpoint = torch.load(chkpt_path)
        self.dcrnn_model.load_state_dict(checkpoint['model_state_dict'])
        #self.optimizer.load_state_dict(checkpoint['optimizer'])
        self.lr_scheduler.load_state_dict(checkpoint['scheduler'])
        batches_seen = checkpoint['batches_seen']
        epoch_num = checkpoint['epoch']
        if self.infer == False:
            self._logger.info("Loaded model at epoch {}".format(epoch_num))
        return batches_seen, epoch_num + 1

    def _setup_graph(self):
        with torch.no_grad():
            self.dcrnn_model = self.dcrnn_model.eval()

            val_iterator = self._data['val_loader']

            for _, (x, y) in enumerate(val_iterator):
                x, y = self._prepare_data(x, y)
                output = self.dcrnn_model(x)
                break

    def train(self, **kwargs):
        kwargs.update(self._train_kwargs)
        return self._train(**kwargs)

    def inference(self, x):
        x = x.squeeze(1)
        x = x.view(self.seq_len, 1, self.num_nodes * self.input_dim)
        output = self.dcrnn_model(x)
        output = self.standard_scaler.inverse_transform(output)
        return output

    def evaluate(self, dataset='val', epoch=0, batches_seen=0):
        """
        Computes mean L1Loss
        :return: mean L1Loss
        """
        with torch.no_grad():
            self.dcrnn_model = self.dcrnn_model.eval()

            val_iterator = self._data['{}_loader'.format(dataset)]
            losses = []
            rmses = []
            masked_rmses = []
            mapes = []
            maes = []
            masked_maes = []

            y_truths = []
            y_preds = []

            for _, (x, y) in enumerate(val_iterator):
                x, y = self._prepare_data(x, y)

                output = self.dcrnn_model(x)
                y_truth = y
                y_pred = output

                loss = self._compute_loss(y_truth, y_pred)

                y_truth = self.standard_scaler.inverse_transform(y_truth)
                y_pred = self.standard_scaler.inverse_transform(y_pred)

                rmse = self._compute_rmse(y_truth, y_pred)
                masked_rmse = self._compute_masked_rmse(y_truth, y_pred)
                mape = self._compute_mape(y_truth, y_pred)
                mae = self._compute_mae(y_truth, y_pred)
                masked_mae = self._compute_masked_mae(y_truth, y_pred)
                losses.append(loss.item())
                rmses.append(rmse.item())
                masked_rmses.append(masked_rmse.item())
                mapes.append(mape.item())
                maes.append(mae.item())
                masked_maes.append(masked_mae.item())

                y_truths.append(y_truth.cpu())
                y_preds.append(y_pred.cpu())

            mean_loss = np.mean(losses)
            mean_rmse = np.mean(rmses)
            mean_masked_rmse = np.mean(masked_rmses)
            mean_mape = np.mean(mapes)
            mean_mae = np.mean(maes)
            mean_masked_mae = np.mean(masked_maes)

            self._writer.add_scalar('{}/loss'.format(dataset), mean_loss, epoch)
            self._writer.add_scalar('{}/RMSE'.format(dataset), mean_rmse, epoch)
            self._writer.add_scalar('{}/MASKED_RMSE'.format(dataset), mean_masked_rmse, epoch)
            self._writer.add_scalar('{}/MAPE'.format(dataset), mean_mape, epoch)
            self._writer.add_scalar('{}/MAE'.format(dataset), mean_mae, epoch)
            self._writer.add_scalar('{}/MASKED_MAE'.format(dataset), mean_masked_mae, epoch)

            y_preds = np.concatenate(y_preds, axis=1)
            y_truths = np.concatenate(y_truths, axis=1)  # concatenate on batch dimension

            y_truths_scaled = []
            y_preds_scaled = []
            for t in range(y_preds.shape[0]):
                # y_truth = self.standard_scaler.inverse_transform(y_truths[t])
                # y_pred = self.standard_scaler.inverse_transform(y_preds[t])
                y_truth = y_truths[t]
                y_pred = y_preds[t]
                y_truths_scaled.append(y_truth)
                y_preds_scaled.append(y_pred)

            # def gen_plot(truth, pred):
            #     """Create a pyplot plot and save to buffer."""
            #     fig = plt.figure(figsize=(60,8))
            #     list_len = len(truth)
            #     x1 = np.arange(0, list_len//2)
            #     x2 = np.arange(list_len//2+1, list_len)
            #     ax1 = fig.add_subplot(2, 1, 1)
            #     ax2 = fig.add_subplot(2, 1, 2)
            #     ax1.plot(x1, truth[:list_len//2], color='orange')
            #     ax1.plot(x1, pred[:list_len//2], 'b-')
            #     ax2.plot(x2, truth[list_len//2+1:], color='orange')
            #     ax2.plot(x2, pred[list_len//2+1:], 'b-')
            #
            #     ax1.set_xticks(np.arange(0, list_len//2, 24))
            #     ax2.set_xticks(np.arange(list_len//2+1, list_len, 24))
            #
            #     plt.tight_layout()
            #     buf = io.BytesIO()
            #     plt.savefig(buf, format='jpeg')
            #     buf.seek(0)
            #     plt.close()
            #     return buf
            
            def gen_plot(truth, pred):
                """Create a pyplot plot and save to buffer."""
                plt.figure(figsize=(20, 3))
                plt.plot(truth, color='orange')
                plt.plot(pred, 'b-')
                buf = io.BytesIO()
                plt.savefig(buf, format='jpeg')
                buf.seek(0)
                plt.close()
                return buf


            def gen_plot_gt(truth):
                """Create a pyplot plot and save to buffer."""
                plt.figure(figsize=(20,3))
                plt.plot(truth, color='orange')
                buf = io.BytesIO()
                plt.savefig(buf, format='jpeg')
                buf.seek(0)
                plt.close()
                return buf

            id = random.randint(0, y_truths.shape[2]-1)

            self._writer.add_image('data', ToTensor()(PIL.Image.open(gen_plot_gt(y_truths[0][::4, id]))), epoch)
            #self._writer.add_image('data1', ToTensor()(PIL.Image.open(gen_plot_gt(y_truths[0][::24, 100]))), epoch)
            self._writer.add_image('result/0', ToTensor()(PIL.Image.open(gen_plot(y_truths[0][::4, id], y_preds[0][::4, id]))), epoch)
            self._writer.add_image('result/3', ToTensor()(PIL.Image.open(gen_plot(y_truths[3][::4, id], y_preds[3][::4, id]))), epoch)
            self._writer.add_image('result/6', ToTensor()(PIL.Image.open(gen_plot(y_truths[6][::4, id], y_preds[6][::4, id]))), epoch)
            #self._writer.add_image('result2/0', ToTensor()(PIL.Image.open(gen_plot(y_truths[0][::24, 100], y_preds[0][::24, 100]))),epoch)
            #self._writer.add_image('result2/3', ToTensor()(PIL.Image.open(gen_plot(y_truths[3][::4, 100], y_preds[3][::4, 100]))),epoch)
            #self._writer.add_image('result2/5', ToTensor()(PIL.Image.open(gen_plot(y_truths[5][::4, 100], y_preds[5][::4, 100]))),epoch)
            return mean_loss, {'prediction': y_preds_scaled, 'truth': y_truths_scaled}

    def _train(self, base_lr,
               steps, patience=50, epochs=100, lr_decay_ratio=0.1, log_every=10, save_model=1,
               test_every_n_epochs=10, epsilon=1e-8, **kwargs):
        # steps is used in learning rate - will see if need to use it?
        min_val_loss = float('inf')
        val_loss = min_val_loss
        wait = 0
        optimizer = self.optimizer
        lr_scheduler = self.lr_scheduler
        self._logger.info('Start training ...')

        # this will fail if model is loaded with a changed batch_size
        # num_batches = self._data['train_loader']
        # self._logger.info("num_batches:{}".format(num_batches))

        batches_seen = self.batches_seen

        for epoch_num in range(self._epoch_num, epochs):

            self.dcrnn_model.train()

            train_iterator = self._data['train_loader']
            losses = []

            start_time = time.time()
            with tqdm(total = len(train_iterator), file=sys.stdout) as pbar:
                for _, (x, y) in enumerate(train_iterator):
                    pbar.update(1)

                    x, y = self._prepare_data(x, y)

                    output = self.dcrnn_model(x, y, batches_seen, pbar)

                    if batches_seen == 0:
                        # this is a workaround to accommodate dynamically registered parameters in DCGRUCell
                        optimizer = torch.optim.Adam(self.dcrnn_model.parameters(), lr=base_lr)

                    loss = self._compute_loss(y, output)

                    self._logger.debug(loss.item())

                    self._writer.add_scalar('training loss',
                                            loss.item(),
                                            batches_seen)

                    losses.append(loss.item())

                    batches_seen += 1
                    optimizer.zero_grad()
                    loss.backward()

                    # gradient clipping - this does it in place
                    torch.nn.utils.clip_grad_norm_(self.dcrnn_model.parameters(), self.max_grad_norm)

                    optimizer.step()

            self._logger.info("epoch complete")
            lr_scheduler.step()
            #self._logger.info("evaluating now!")

            #val_loss, _ = self.evaluate(dataset='val', epoch=epoch_num, batches_seen=batches_seen)

            end_time = time.time()

            self._writer.add_scalar('mean training loss',
                                    np.mean(losses),
                                    epoch_num)

            if (epoch_num % log_every) == log_every - 1:
                self._logger.info("evaluating now!")
                val_loss, _ = self.evaluate(dataset='val', epoch=epoch_num, batches_seen=batches_seen)
                message = 'Epoch [{}/{}] ({}) train_loss: {:.4f}, val_loss: {:.4f}, lr: {:.8f}, ' \
                          '{:.1f}s'.format(epoch_num, epochs, batches_seen,
                                           np.mean(losses), val_loss, lr_scheduler.get_last_lr()[0],
                                           (end_time - start_time))
                self._logger.info(message)

            # if (epoch_num % test_every_n_epochs) == test_every_n_epochs - 1:
            #     test_loss, _ = self.evaluate(dataset='test', epoch=epoch_num, batches_seen=batches_seen)
            #     message = 'Epoch [{}/{}] ({}) train_loss: {:.4f}, test_loss: {:.4f},  lr: {:.8f}, ' \
            #               '{:.1f}s'.format(epoch_num, epochs, batches_seen,
            #                                np.mean(losses), test_loss, lr_scheduler.get_last_lr()[0],
            #                                (end_time - start_time))
            #     self._logger.info(message)

            if val_loss < min_val_loss:
                wait = 0
                if save_model:
                    model_file_name = self.save_model(batches_seen, epoch_num, lr_scheduler)
                    self._logger.info(
                        'Val loss decrease from {:.4f} to {:.4f}, '
                        'saving to {}'.format(min_val_loss, val_loss, model_file_name))
                min_val_loss = val_loss

            elif val_loss > min_val_loss:
                wait += 1
                if wait == patience:
                    model_file_name = self.save_model(batches_seen, epoch_num, lr_scheduler)
                    self._logger.warning('Early stopping at epoch: %d' % epoch_num)
                    break

    def _prepare_data(self, x, y):
        x, y = self._get_x_y(x, y)
        x, y = self._get_x_y_in_correct_dims(x, y)
        return x.to(device), y.to(device)

    def _get_x_y(self, x, y):
        """
        :param x: shape (batch_size, seq_len, num_sensor, input_dim)
        :param y: shape (batch_size, horizon, num_sensor, input_dim)
        :returns x shape (seq_len, batch_size, num_sensor, input_dim)
                 y shape (horizon, batch_size, num_sensor, input_dim)
        """
        # self._logger.debug("X: {}".format(x.size()))
        # self._logger.debug("y: {}".format(y.size()))
        x = x.permute(1, 0, 2, 3)
        y = y.permute(1, 0, 2, 3)
        return x, y

    # def scaler_mean(self, x, y):
    #     scale = (1 + torch.mean(x[:, :, :, 0], dim=0))
    #     scale_x = scale.repeat(self.seq_len, 1, 1)
    #     scale_y = scale.repeat(self.horizon, 1, 1)
    #     x[:, :, :, 0] = x[:, :, :, 0]/scale_x
    #     y[:, :, :, 0] = y[:, :, :, 0]/scale_y
    #     return x, y, scale_y

    def _get_x_y_in_correct_dims(self, x, y):
        """
        :param x: shape (seq_len, batch_size, num_sensor, input_dim)
        :param y: shape (horizon, batch_size, num_sensor, input_dim)
        :return: x: shape (seq_len, batch_size, num_sensor * input_dim)
                 y: shape (horizon, batch_size, num_sensor * output_dim)
        """
        batch_size = x.size(1)
        x = x.view(self.seq_len, batch_size, self.num_nodes * self.input_dim)
        y = y[..., 1:2].view(self.horizon, batch_size,
                                          self.num_nodes * self.output_dim)
        return x, y

    def _compute_loss(self, y_true, y_predicted):
        # y_true = self.standard_scaler.inverse_transform(y_true)
        # y_predicted = self.standard_scaler.inverse_transform(y_predicted)
        # y_true = y_true
        # y_predicted = y_predicted
        return masked_loss(y_predicted, y_true)

    def _compute_rmse(self, y_true, y_predicted):
        return torch.sqrt(torch.mean(torch.square(y_predicted - y_true)))

    def _compute_masked_rmse(self, y_true, y_predicted):
        mask = y_true != 0
        return torch.sqrt(torch.mean(torch.square(y_predicted - y_true)[mask]))

    def _compute_mape(self, y_true, y_predicted):
        mask = y_true != 0
        return (torch.abs(1 - y_predicted/y_true))[mask].mean()

    def _compute_mae(self, y_true, y_predicted):
        return (torch.abs(y_true - y_predicted)).mean()

    def _compute_masked_mae(self, y_true, y_predicted):
        mask = y_true != 0
        return (torch.abs(y_true - y_predicted))[mask].mean()
