import sys
import grpc
import io
import numpy as np
import argparse
import json

from softberry_pb2_grpc import SoftberryStub
from softberry_pb2 import Json


class SoftberryClient(object):
    def __init__(self, remote='127.0.0.1:53007', chunk_size=1048576):
        channel = grpc.insecure_channel(remote)
        self.stub = SoftberryStub(channel)
        self.chunk_size = chunk_size

    def predict(self, input):
        message = self._generate_json_binary_iterator(input)
        return self.stub.Predict(message)

    def _generate_json_binary_iterator(self, input):
        for idx in range(0, len(input), self.chunk_size):
            yield Json(data=input[idx:idx + self.chunk_size])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Voicefilter client')
    parser.add_argument('-r', '--remote', type=str, default='127.0.0.1:53007',
                        help='grpc: ip:port')
    parser.add_argument('-i', '--input', type=str, default='input.json',
                        help='input file')
    args = parser.parse_args()

    client = SoftberryClient(remote=args.remote)

    with open(args.input, 'r') as j:
        input = json.dumps(json.loads(j.read()), indent=2).encode('utf-8')
    output = client.predict(input)

    with open('output_sample.json', 'w') as outfile:
        json.dump(json.load(io.BytesIO(output.data)), outfile)
    print('Prediction saved as output_sample.json.')
