import argparse
import io
import numpy as np
import torch
import os
import sys
import yaml
import json
import pandas as pd

from lib.utils import load_graph_data, StandardScaler
from model.dcrnn_model import DCRNNModel

from softberry_pb2_grpc import SoftberryServicer
from softberry_pb2 import Json

class SoftberryInference(SoftberryServicer):
    def __init__(self, args):
        with open(args.config) as f:
            self.config = yaml.safe_load(f)
        checkpoint = torch.load(args.model, map_location='cpu')

        torch.cuda.set_device(args.device)
        self.device = args.device
        self._model_kwargs = self.config.get('model')
        graph_pkl_filename = self.config['data'].get('graph_pkl_filename')
        self.sensor_ids, self.sensor_id_to_ind, adj_mx = load_graph_data(graph_pkl_filename)

        mean_std = np.load(os.path.join(self.config.get('data').get('dataset_dir'), 'mean_std.npz'))
        mean, std = round(float(mean_std['mean']), 3), round(float(mean_std['std']), 3)
        self.standard_scaler = StandardScaler(mean, std)

        ride_count_mean, ride_count_std = round(float(mean_std['ride_count_mean']), 3), round(float(mean_std['ride_count_std']), 3)
        self.ride_count_standard_scaler = StandardScaler(ride_count_mean, ride_count_std)

        stop_count_mean, stop_count_std = round(float(mean_std['stop_count_mean']), 3), round(float(mean_std['stop_count_std']), 3)
        self.stop_count_standard_scaler = StandardScaler(stop_count_mean, stop_count_std)

        self.model = DCRNNModel(adj_mx, **self._model_kwargs).cuda()
        self.model.load_state_dict(checkpoint['model_state_dict'], strict=False)
        self.model.eval()

        self.num_nodes = int(self._model_kwargs.get('num_nodes', 1))
        self.input_dim = int(self._model_kwargs.get('input_dim', 1))
        self.seq_len = int(self._model_kwargs.get('seq_len'))  # for the encoder
        self.output_dim = int(self._model_kwargs.get('output_dim', 1))
        self.horizon = int(self._model_kwargs.get('horizon', 1))

        torch.cuda.empty_cache()

    def Predict(self, input, context):
        json_input = bytearray()
        for input_iterator in input:
            json_input.extend(input_iterator.data)

        input = json.load(io.BytesIO(json_input))

        x = np.zeros((self.seq_len, self.num_nodes, self.input_dim))
        for i in range(self.seq_len):
            x[i, :, 5] = np.sin((2 * np.pi / 24) * input[i]['time'])
            x[i, :, 6] = np.sin((2 * np.pi / 7) * input[i]['week'])
            x[i, :, 7] = np.sin((2 * np.pi / 12) * input[i]['month'])
            for j in range(self.num_nodes):
                x[i, j, 0] = input[i][self.sensor_ids[j]]['charge_count']
                x[i, j, 1] = self.standard_scaler.transform(input[i][self.sensor_ids[j]]['minutes_of_charging'])
                x[i, j, 2] = input[i][self.sensor_ids[j]]['occupy_count']
                x[i, j, 3] = self.ride_count_standard_scaler.transform(input[i][self.sensor_ids[j]]['ride_count'])
                x[i, j, 4] = self.stop_count_standard_scaler.transform(input[i][self.sensor_ids[j]]['stop_count'])
                x[i, j, input[i][self.sensor_ids[j]]['charger_type'] + 7] = 1
                if input[i][self.sensor_ids[j]]['pay'] == 'Y':
                    x[i, j, 20] = 1
                if input[i][self.sensor_ids[j]]['park'] == 'Y':
                    x[i, j, 21] = 1
                if input[i][self.sensor_ids[j]]['roof'] == 'Y':
                    x[i, j, 22] = 1

        output_list = []
        torch.cuda.set_device(self.device)
        with torch.no_grad():
            x = torch.from_numpy(x).float().to(self.device)
            x = x.view(self.seq_len, 1, self.num_nodes * self.input_dim)
            output = self.model(x)
            output = self.standard_scaler.inverse_transform(output)

            for t in range(self.horizon - 1):
                output_t = output[t+1].squeeze(0)  # self.num_nodes

                dict = {}
                for i in range(len(self.sensor_ids)):
                    dict[self.sensor_ids[i]] = torch.clamp(output_t[i], min=0.0).item()
                output_list.append(dict)

            output = json.dumps(output_list).encode('utf-8')
            return Json(data=output)