FROM pytorch/pytorch:1.6.0-cuda10.1-cudnn7-devel

COPY ./ .
RUN pip install -r requirements.txt
RUN apt-get update
RUN apt-get install -y vim
RUN pip install tensorboard==2.3.0
RUN apt-get install -y git

