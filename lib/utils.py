import logging
import numpy as np
import os
import glob
import pickle
import scipy.sparse as sp
import sys
import torch
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader, WeightedRandomSampler

from scipy.sparse import linalg

class old_DataLoader(object):
    def __init__(self, xs, ys, batch_size, pad_with_last_sample=True, shuffle=False):
        """

        :param xs:
        :param ys:
        :param batch_size:
        :param pad_with_last_sample: pad with the last sample to make number of samples divisible to batch_size.
        """
        self.batch_size = batch_size
        self.current_ind = 0
        if pad_with_last_sample:
            num_padding = (batch_size - (len(xs) % batch_size)) % batch_size
            x_padding = np.repeat(xs[-1:], num_padding, axis=0)
            y_padding = np.repeat(ys[-1:], num_padding, axis=0)
            xs = np.concatenate([xs, x_padding], axis=0)
            ys = np.concatenate([ys, y_padding], axis=0)
        self.size = len(xs)
        self.num_batch = int(self.size // self.batch_size)
        if shuffle:
            permutation = np.random.permutation(self.size)
            xs, ys = xs[permutation], ys[permutation]
        self.xs = xs
        self.ys = ys

    def get_iterator(self):
        self.current_ind = 0

        def _wrapper():
            while self.current_ind < self.num_batch:
                start_ind = self.batch_size * self.current_ind
                end_ind = min(self.size, self.batch_size * (self.current_ind + 1))
                x_i = self.xs[start_ind: end_ind, ...]
                y_i = self.ys[start_ind: end_ind, ...]
                yield (x_i, y_i)
                self.current_ind += 1

        return _wrapper()

def create_dataloader(category, dataset_dir, batch_size):
    def collate_fn(batch):
        x_list = list()
        y_list = list()
        for x, y in batch:
            x_list.append(x)
            y_list.append(y)

        x_list = torch.stack(x_list, dim=0)
        y_list = torch.stack(y_list, dim=0)
        return (x_list, y_list)

    def WR_sampler(dataset):
        weight_list = list()
        for x, _ in dataset:
            weight = 1 + torch.mean(x[:,:,1])
            weight_list.append(weight)

        #weight_list = torch.stack(weight_list, dim=0)
        return WeightedRandomSampler(
            weights = weight_list,
            num_samples = len(weight_list),
            replacement = True)

    if category=='train':
        dataset_train = softDataset('train', dataset_dir)
        return DataLoader(dataset=dataset_train,
                          batch_size=batch_size,
                          shuffle=False,
                          num_workers=16,
                          collate_fn=collate_fn,
                          pin_memory=True,
                          drop_last=True,
                          sampler=WR_sampler(dataset_train))
    elif category=='val':
        return DataLoader(dataset=softDataset('val', dataset_dir),
                          collate_fn=collate_fn,
                          batch_size=batch_size, shuffle=False, num_workers=0)
    # else:
    #     return DataLoader(dataset=softDataset('test', dataset_dir),
    #                       collate_fn=collate_fn,
    #                       batch_size=batch_size, shuffle=False, num_workers=0)

class softDataset(Dataset):
    def __init__(self, category, dataset_dir):
        self.category = category
        if category == 'train':
            self.train_files = glob.glob(os.path.join(dataset_dir, 'train', '*.npz'))
        else:
            with np.load(os.path.join(dataset_dir, category + '.npz')) as cat_data:
                self.data_x = cat_data['x']
                self.data_y = cat_data['y']

    def __len__(self):
        if self.category == 'train':
            return len(self.train_files)
        else:
            return len(self.data_x)

    def __getitem__(self, idx):
        if self.category == 'train':
            with np.load(self.train_files[idx]) as train_file:
                data_x = train_file['x']
                data_y = train_file['y']
            #random_x = np.random.randn(data_x.shape[0], data_x.shape[1])
            #random_y = np.random.randn(data_y.shape[0], data_y.shape[1])
            #data_x[:,:,0] = data_x[:,:,0] + random_x
            #data_y[:,:,0] = data_y[:,:,0] + random_y
            #data_x[:,:,0][data_x[:,:,0]<0]=0
            #data_y[:,:,0][data_y[:,:,0]<0]=0
            return torch.from_numpy(data_x).float(), torch.from_numpy(data_y).float()
        else:
            return torch.from_numpy(self.data_x[idx, ...]).float(), torch.from_numpy(self.data_y[idx, ...]).float()

class StandardScaler:
    """
    Standard the input
    """

    def __init__(self, mean, std):
        self.mean = mean
        self.std = std

    def transform(self, data):
        return (data - self.mean) / self.std

    def inverse_transform(self, data):
        return (data * self.std) + self.mean


def add_simple_summary(writer, names, values, global_step):
    """
    Writes summary for a list of scalars.
    :param writer:
    :param names:
    :param values:
    :param global_step:
    :return:
    """
    for name, value in zip(names, values):
        #summary = SummaryWriter()
        #summary.add_scalar(name, value, global_step)
        summary = tf.Summary()
        summary_value = summary.value.add()
        summary_value.simple_value = value
        summary_value.tag = name
        writer.add_summary(summary, global_step)


def calculate_normalized_laplacian(adj):
    """
    # L = D^-1/2 (D-A) D^-1/2 = I - D^-1/2 A D^-1/2
    # D = diag(A 1)
    :param adj:
    :return:
    """
    adj = sp.coo_matrix(adj)
    d = np.array(adj.sum(1))
    d_inv_sqrt = np.power(d, -0.5).flatten()
    d_inv_sqrt[np.isinf(d_inv_sqrt)] = 0.
    d_mat_inv_sqrt = sp.diags(d_inv_sqrt)
    normalized_laplacian = sp.eye(adj.shape[0]) - adj.dot(d_mat_inv_sqrt).transpose().dot(d_mat_inv_sqrt).tocoo()
    return normalized_laplacian


def calculate_random_walk_matrix(adj_mx):
    adj_mx = sp.coo_matrix(adj_mx)
    d = np.array(adj_mx.sum(1))
    d_inv = np.power(d, -1).flatten()
    d_inv[np.isinf(d_inv)] = 0.
    d_mat_inv = sp.diags(d_inv)
    random_walk_mx = d_mat_inv.dot(adj_mx).tocoo()
    return random_walk_mx


def calculate_reverse_random_walk_matrix(adj_mx):
    return calculate_random_walk_matrix(np.transpose(adj_mx))


def calculate_scaled_laplacian(adj_mx, lambda_max=2, undirected=True):
    if undirected:
        adj_mx = np.maximum.reduce([adj_mx, adj_mx.T])
    L = calculate_normalized_laplacian(adj_mx)
    if lambda_max is None:
        lambda_max, _ = linalg.eigsh(L, 1, which='LM')
        lambda_max = lambda_max[0]
    L = sp.csr_matrix(L)
    M, _ = L.shape
    I = sp.identity(M, format='csr', dtype=L.dtype)
    L = (2 / lambda_max * L) - I
    return L.astype(np.float32)


def config_logging(log_dir, log_filename='info.log', level=logging.INFO):
    # Add file handler and stdout handler
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # Create the log directory if necessary.
    try:
        os.makedirs(log_dir)
    except OSError:
        pass
    file_handler = logging.FileHandler(os.path.join(log_dir, log_filename))
    file_handler.setFormatter(formatter)
    file_handler.setLevel(level=level)
    # Add console handler.
    console_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(console_formatter)
    console_handler.setLevel(level=level)
    logging.basicConfig(handlers=[file_handler, console_handler], level=level)


def get_logger(log_dir, name, log_filename='info.log', level=logging.INFO):
    logger = logging.getLogger(name)
    logger.setLevel(level)
    # Add file handler and stdout handler
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler = logging.FileHandler(os.path.join(log_dir, log_filename))
    file_handler.setFormatter(formatter)
    # Add console handler.
    console_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(console_formatter)
    logger.addHandler(file_handler)
    logger.addHandler(console_handler)
    # Add google cloud log handler
    logger.info('Log directory: %s', log_dir)
    return logger


def get_total_trainable_parameter_size():
    """
    Calculates the total number of trainable parameters in the current graph.
    :return:
    """
    total_parameters = 0
    for variable in tf.trainable_variables():
        # shape is an array of tf.Dimension
        total_parameters += np.product([x.value for x in variable.get_shape()])
    return total_parameters


def load_dataset(dataset_dir, batch_size, **kwargs):
    data = {}
    data['train_loader'] = create_dataloader('train', dataset_dir, batch_size)
    data['val_loader'] = create_dataloader('val', dataset_dir, batch_size)
    #data['test_loader'] = create_dataloader('test', dataset_dir, test_batch_size)

    return data


def load_graph_data(pkl_filename):
    sensor_ids, sensor_id_to_ind, adj_mx = load_pickle(pkl_filename)
    return sensor_ids, sensor_id_to_ind, adj_mx


def load_pickle(pickle_file):
    try:
        with open(pickle_file, 'rb') as f:
            pickle_data = pickle.load(f)
    except UnicodeDecodeError as e:
        with open(pickle_file, 'rb') as f:
            pickle_data = pickle.load(f, encoding='latin1')
    except Exception as e:
        print('Unable to load data ', pickle_file, ':', e)
        raise
    return pickle_data
