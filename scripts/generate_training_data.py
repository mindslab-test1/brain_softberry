from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import shutil
import argparse
import numpy as np
import os
from glob import glob
import pandas as pd
from tqdm import tqdm


def transform(data, mean, std):
    return (data - mean) / std

def get_data(charge_count, minutes_of_charging, occupy_count, ride_count, stop_count, static_data, charge_ids):
    df = charge_count
    info_df = static_data
    num_samples = charge_count.shape[0]
    num_nodes = len(charge_ids)
    new_ids = [x for x in charge_ids not in info_df.columns]
    data = np.expand_dims(charge_count.loc[:, info_df.columns], axis=-1)
    minutes_of_charging = np.expand_dims(minutes_of_charging.loc[:, info_df.columns], axis=-1)
    occupy_count = np.expand_dims(occupy_count.loc[:, info_df.columns], axis=-1)
    ride_count = np.expand_dims(ride_count.loc[:, info_df.columns], axis=-1)
    stop_count = np.expand_dims(stop_count.loc[:, info_df.columns], axis=-1)

    data_all = np.zeros((data.shape[0], num_nodes, 22), dtype=np.float32)
    data_all[..., 0:1] = data
    data_all[..., 1:2] = minutes_of_charging
    data_all[..., 2:3] = occupy_count
    data_all[..., 3:4] = ride_count
    data_all[..., 4:5] = stop_count

    charger_type = []
    for charger_id in info_df.columns:
        charger_type.append(info_df.loc['type_id', charger_id])
    charger_type = np.array(charger_type, dtype=np.int64)
    charger_type_onehot = np.zeros(shape=(num_samples, num_nodes, 12), dtype=np.int32)
    charger_type_onehot[:, np.arange(num_nodes), charger_type - 1] = 1
    data_all[..., 5:17] = charger_type_onehot
    print('add charger_type done')
    del charger_type, charger_type_onehot

    pay = []
    for charger_id in info_df.columns:
        if info_df.loc['pay', charger_id] == 'Y':
            pay.append(1)
        else:
            pay.append(0)
    pay = np.array(pay, dtype=np.int64)
    data_all[..., 17] = pay
    print('add pay done')

    park = []
    for charger_id in info_df.columns:
        if info_df.loc['park', charger_id] == '1':
            park.append(1)
        else:
            park.append(0)
    park = np.array(park, dtype=np.int64)
    data_all[..., 18] = park
    print('add park done')

    roof = []
    for charger_id in info_df.columns:
        if info_df.loc['roof', charger_id] == '1':
            roof.append(1)
        else:
            roof.append(0)
    roof = np.array(roof, dtype=np.int64)
    data_all[..., 19] = roof
    print('add roof done')

    del pay, park, roof

    # from_start_list = list()
    # for i in range(data.shape[1]):
    #     x = data[:, i, 0]
    #     if len(np.where(x > 0)[0]) == 0 and len(np.where(y > 0)[0]) == 0:
    #         from_start = np.zeros(x.shape[0])
    #     else:
    #         if len(np.where(x > 0)[0]) == 0:
    #             min_index = np.min(np.where(y > 0))
    #         elif len(np.where(y > 0)[0]) == 0:
    #             min_index = np.min(np.where(x > 0))
    #         else:
    #             min_index = min(np.min(np.where(x > 0)), np.min(np.where(y > 0)))
    #         from_start = np.arange(x.shape[0] - min_index) / x.shape[0]
    #         from_start = np.concatenate((np.zeros(min_index), from_start), axis=0)
    #     # print(i,':',from_start,'\n')
    #     from_start_list.append(from_start)
    # from_start = np.stack(from_start_list, axis=1)
    # # from_start = np.arange(num_samples) / num_samples
    # # from_start = np.tile(from_start, [1, num_nodes, 1]).transpose((2, 1, 0))
    # # data_list.append(from_start)
    # data_all[..., 20] = from_start
    # print('add from_start done')
    # del from_start_list, from_start


    df.index = pd.to_datetime(df.index)
    time_ind = (df.index.values - df.index.values.astype("datetime64[D]")) / np.timedelta64(1, "D") * 24
    time_in_day = np.tile(time_ind, [1, num_nodes, 1]).transpose((2, 1, 0))
    period = 24
    time_in_day = np.sin((2 * np.pi / period) * time_in_day)
    # data_list.append(time_in_day)
    data_all[..., 20:21] = time_in_day
    print('add time_in_day done')

    day_in_we = df.index.weekday
    day_in_week = np.tile(day_in_we, [1, num_nodes, 1]).transpose((2, 1, 0))
    period = 7
    day_in_week = np.sin((2 * np.pi / period) * day_in_week)
    # data_list.append(day_in_week)
    data_all[..., 21:22] = day_in_week
    print('add day_in_week done')

    month_in_ye = df.index.month
    month = np.tile(month_in_ye, [1, num_nodes, 1]).transpose((2, 1, 0))
    period = 12
    month = np.sin((2 * np.pi / period) * month)
    # data_list.append(month)
    data_all[..., 22:23] = month
    print('add month done')

    del data, minutes_of_charging, occupy_count, ride_count, stop_count, time_ind, time_in_day, day_in_we, day_in_week, \
        month_in_ye, month

    return data_all


def generate_graph_seq2seq_io_data(
        df, id, x_offsets, y_offsets, unit=4
):
    """
    Generate samples from
    :param df:
    :param x_offsets:
    :param y_offsets:
    :param add_time_in_day:
    :param add_day_in_week:
    :param scaler:
    :return:
    # x: (epoch_size, input_length, num_nodes, input_dim)
    # y: (epoch_size, output_length, num_nodes, output_dim)
    """
    charger_ids = []
    for dir in sorted(glob(os.path.join(df, '*'))):
        if len(glob(os.path.join(dir, str(id), '*', '*', 'static_data.csv'))) != 0:
            static_data = pd.read_csv(glob(os.path.join(dir, str(id), '*', '*', 'static_data.csv'))[0], index_col=0)
            new_ids = [x for x in static_data.columns not in charger_ids]
            charger_ids += new_ids

    data_list = []
    for dir in sorted(glob(os.path.join(df, '*'))):
        print(dir)
        charge_count = pd.read_csv(glob(os.path.join(dir, str(id), '*', '*', 'charge_count.csv'))[0], index_col=0)
        minutes_of_charging = pd.read_csv(glob(os.path.join(dir, str(id), '*', '*', 'minutes_of_charging.csv'))[0],
                                          index_col=0)
        occupy_count = pd.read_csv(glob(os.path.join(dir, str(id), '*', '*', 'occupy_count.csv'))[0], index_col=0)
        ride_count = pd.read_csv(glob(os.path.join(dir, str(id), '*', '*', 'ride_count.csv'))[0], index_col=0)
        stop_count = pd.read_csv(glob(os.path.join(dir, str(id), '*', '*', 'stop_count.csv'))[0], index_col=0)

        if len(glob(os.path.join(dir, str(id), '*', '*', 'static_data.csv'))) != 0:
            static_data = pd.read_csv(glob(os.path.join(dir, str(id), '*', '*', 'static_data.csv'))[0], index_col=0)
        data_all = get_data(charge_count, minutes_of_charging, occupy_count, ride_count, stop_count, static_data, charger_ids)
        data_list.append(data_all)
    data_all = np.concatenate(data_list, axis=0)

    # epoch_len = num_samples + min(x_offsets) - max(y_offsets)
    # x, y = [], []
    # t is the index of the last observation.
    min_t = abs(min(x_offsets))
    max_t = abs(data_all.shape[0] - abs(max(y_offsets)))  # Exclusive
    x = np.zeros((max_t - min_t, 42, data_all.shape[1], data_all.shape[2]), dtype=np.float32)
    y = np.zeros((max_t - min_t, 7, data_all.shape[1], data_all.shape[2]), dtype=np.float32)
    pbar = tqdm(total=max_t - min_t)
    for t in range(min_t, max_t):
        if t % 10 == 0:
            pbar.update(10)
        x_t1 = data_all[t + x_offsets, ...]
        y_t1 = data_all[t + y_offsets, ...]
        x_t = np.zeros((42, x_t1.shape[1], x_t1.shape[2]), dtype=np.float32)
        y_t = np.zeros((7, y_t1.shape[1], y_t1.shape[2]), dtype=np.float32)
        for i in range(42):
            x_t[i, :, 0] = np.sum(x_t1[i * unit:(i + 1) * unit, :, 0], axis=0)
            x_t[i, :, 1] = np.sum(x_t1[i * unit:(i + 1) * unit, :, 1], axis=0)
            x_t[i, :, 2] = np.sum(x_t1[i * unit:(i + 1) * unit, :, 2], axis=0)
            x_t[i, :, 3] = np.sum(x_t1[i * unit:(i + 1) * unit, :, 3], axis=0)
            x_t[i, :, 4] = np.sum(x_t1[i * unit:(i + 1) * unit, :, 4], axis=0)
            x_t[i, :, np.arange(5, 22)] = x_t1[i * unit, :, np.arange(5, 22)]
        for i in range(7):
            y_t[i, :, 0] = np.sum(y_t1[i * unit:(i + 1) * unit, :, 0], axis=0)
            y_t[i, :, 1] = np.sum(y_t1[i * unit:(i + 1) * unit, :, 1], axis=0)
            y_t[i, :, 2] = np.sum(y_t1[i * unit:(i + 1) * unit, :, 2], axis=0)
            y_t[i, :, 3] = np.sum(y_t1[i * unit:(i + 1) * unit, :, 3], axis=0)
            y_t[i, :, 4] = np.sum(y_t1[i * unit:(i + 1) * unit, :, 4], axis=0)
            y_t[i, :, np.arange(5, 22)] = y_t1[i * unit, :, np.arange(5, 22)]
        x[t - min_t, ...] = x_t
        y[t - min_t, ...] = y_t
        del x_t1, y_t1, x_t, y_t
    pbar.close()

    data_mean, data_std = round(y[..., 1].mean(),3), round(y[..., 1].std(),3)
    x[..., 0] = transform(x[..., 0], round(y[...,0].mean(),3), round(y[...,0].std(),3))
    x[..., 1] = transform(x[..., 1], round(y[...,1].mean(),3), round(y[...,1].std(),3))
    x[..., 2] = transform(x[..., 2], round(y[...,2].mean(),3), round(y[...,2].std(),3))
    x[..., 3] = transform(x[..., 3], round(y[...,3].mean(),3), round(y[...,3].std(),3))
    x[..., 4] = transform(x[..., 4], round(y[...,4].mean(),3), round(y[...,4].std(),3))

    y[..., 0] = transform(y[..., 0], round(y[..., 0].mean(), 3), round(y[..., 0].std(), 3))
    y[..., 1] = transform(y[..., 1], round(y[..., 1].mean(), 3), round(y[..., 1].std(), 3))
    y[..., 2] = transform(y[..., 2], round(y[..., 2].mean(), 3), round(y[..., 2].std(), 3))
    y[..., 3] = transform(y[..., 3], round(y[..., 3].mean(), 3), round(y[..., 3].std(), 3))
    y[..., 4] = transform(y[..., 4], round(y[..., 4].mean(), 3), round(y[..., 4].std(), 3))

    print('mean: ', data_mean)
    print('std: ', data_std)
    return x, y, data_mean, data_std


def generate_train_val_test(args):
    # 0 is the latest observed sample.
    x_offsets = np.sort(
        # np.concatenate(([-week_size + 1, -day_size + 1], np.arange(-11, 1, 1)))
        np.concatenate((np.arange(-167, 1, 1),))
    )
    # Predict the next one hour
    y_offsets = np.sort(np.arange(1, 29, 1))
    # x: (num_samples, input_length, num_nodes, input_dim)
    # y: (num_samples, output_length, num_nodes, output_dim)
    x, y, data_mean, data_std = generate_graph_seq2seq_io_data(
        args.df_filename,
        id=args.id,
        x_offsets=x_offsets,
        y_offsets=y_offsets,
        unit=args.unit,
    )

    print("x shape: ", x.shape, ", y shape: ", y.shape)
    # Write the data into npz file.
    # num_test = 6831, using the last 6831 examples as testing.
    # for the rest: 7/8 is used for training, and 1/8 is used for validation.
    num_samples = x.shape[0]
    num_test = round(num_samples * 0.001)
    num_train = round(num_samples * 0.9)
    num_val = num_samples - num_test - num_train

    # train
    x_train, y_train = x[:num_train], y[:num_train]
    # val
    x_val, y_val = (
        x[num_train: num_train + num_val],
        y[num_train: num_train + num_val],
    )
    # test
    x_test, y_test = x[-num_test:], y[-num_test:]

    print('start saving files for training')
    # save train files each
    train_path = os.path.join(args.output_dir, 'train')
    if os.path.isdir(train_path):
        shutil.rmtree(train_path)
    os.makedirs(train_path, exist_ok=True)
    np.savez(os.path.join(args.output_dir, 'mean_std.npz'), mean=data_mean, std=data_std)
    pbar = tqdm(total=x_train.shape[0])
    for i in range(x_train.shape[0]):
        if i % 10 == 0:
            pbar.update(10)
        np.savez(os.path.join(train_path, 'train_%d.npz' % i), x=x[i, ...], y=y[i, ...])
    pbar.close()
    print('end saving files for training, start saving files for val, test')
    # save val, test
    for cat in ["val", "test"]:
        _x, _y = locals()["x_" + cat], locals()["y_" + cat]
        print(cat, "x: ", _x.shape, "y:", _y.shape)
        np.savez(
            os.path.join(args.output_dir, "%s.npz" % cat),
            x=_x,
            y=_y,
            # x_offsets=x_offsets.reshape(list(x_offsets.shape) + [1]),
            # y_offsets=y_offsets.reshape(list(y_offsets.shape) + [1]),
        )


def main(args):
    print(args.output_dir)
    print("Generating training data")
    generate_train_val_test(args)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--output_dir", type=str, default="/DATA1/softberry_preprocessed_try/50110", help="Output directory.",
    )
    parser.add_argument(
        "-d", "--df_filename",
        type=str,
        default="/DATA1/softberry",
        help="Raw data readings.",
    )
    parser.add_argument(
        "--id",
        type=int,
        default=5011000000,
        help="id of the target region"
    )
    parser.add_argument(
        "-u", "--unit",
        type=int,
        default=4,
        help="unit",
    )

    args = parser.parse_args()
    main(args)
