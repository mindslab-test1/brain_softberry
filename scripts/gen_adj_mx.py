from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import numpy as np
import pandas as pd
import pickle
from haversine import haversine

def get_adjacency_matrix(info_df, kappa=0.95):
    """

    :param info_df: data frame with [longitude, latitude, type].
    :param normalized_k: entries that become lower than normalized_k after normalization are set to zero for sparsity.
    :return:
    """
    charger_ids = info_df.columns
    num_chargers = len(charger_ids)
    dist_mx = np.zeros((num_chargers, num_chargers), dtype=np.float32)
    dist_mx[:] = np.inf

    # Builds sensor id to index map.
    charger_id_to_ind = {}
    for i, charger_id in enumerate(charger_ids):
        charger_id_to_ind[charger_id] = i

    # Fills cells in the matrix with distances.
    for s in charger_ids:
        s_loc = (float(info_df[s][1]), float(info_df[s][0]))
        for e in charger_ids:
             e_loc = (float(info_df[e][1]), float(info_df[e][0]))
             dist_mx[charger_id_to_ind[s], charger_id_to_ind[e]] = haversine(s_loc, e_loc, unit='km')

    # Calculates the standard deviation as theta.
    distances = dist_mx[~np.isinf(dist_mx)].flatten()
    std = distances.std()
    adj_mx = np.exp(-np.square(dist_mx / std))
    # Make the adjacent matrix symmetric by taking the max.
    # adj_mx = np.maximum.reduce([adj_mx, adj_mx.T])

    # Sets entries that lower than a threshold, i.e., k, to zero for sparsity.
    normalized_k = np.exp(-np.square(kappa / std))
    print(kappa, normalized_k)
    print(adj_mx.shape)
    print(adj_mx[adj_mx<normalized_k].shape)
    print(adj_mx[adj_mx>=normalized_k].shape)
    adj_mx[adj_mx < normalized_k] = 0
    return charger_ids, charger_id_to_ind, adj_mx


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--charger_info', type=str, default='/DATA1/softberry_0802/5013000000/2021/3m4m5m/static_data.csv',
                        help='CSV file containing charger id, longitude, latitude, type. etc')
    parser.add_argument('--kappa', type=float, default=3,
                        help='Entries that become lower than normalized_k after normalization are set to zero for sparsity.')
    parser.add_argument('--output_pkl_filename', type=str, default='/DATA1/softberry_preprocessed_345/50130/adj_mx.pkl',
                        help='Path of the output file.')
    args = parser.parse_args()

    info_df = pd.read_csv(args.charger_info, index_col=0)
    charger_ids, charger_id_to_ind, adj_mx = get_adjacency_matrix(info_df, args.kappa)
    # Save to pickle file.
    with open(args.output_pkl_filename, 'wb') as f:
        pickle.dump([charger_ids, charger_id_to_ind, adj_mx], f, protocol=2)
