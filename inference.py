import argparse
import numpy as np
import torch
import os
import sys
import yaml
import json

from lib.utils import load_graph_data
from model.dcrnn_supervisor import DCRNNSupervisor


def run_dcrnn(args):
    with open(args.config_filename) as f:
        supervisor_config = yaml.safe_load(f)

        graph_pkl_filename = supervisor_config['data'].get('graph_pkl_filename')
        sensor_ids, sensor_id_to_ind, adj_mx = load_graph_data(graph_pkl_filename)

        supervisor = DCRNNSupervisor(adj_mx=adj_mx, chkpt_path=args.checkpoint_path, infer=True, **supervisor_config)

        x = np.load(args.input)['x']
        x = torch.from_numpy(x).float().cuda()
        output = supervisor.inference(x) # self.horizon, batch_size, self.num_nodes
        output = output[6].squeeze(0) # self.num_nodes
        dict = {}
        for i in range(len(sensor_ids)):
            dict[sensor_ids[i]] = torch.clamp(output[i], min=0.0).item()
        # np.savez_compressed(args.output_filename, output.detach().cpu())
        print(dict)
        with open(args.output_filename, 'w') as outfile:
            json.dump(dict, outfile)
        print('Prediction saved as {}.'.format(args.output_filename))


if __name__ == '__main__':
    sys.path.append(os.getcwd())
    parser = argparse.ArgumentParser()
    parser.add_argument('--use_cpu_only', default=False, type=str, help='Whether to run on cpu.')
    parser.add_argument('--config_filename', default='data/soft.yaml', type=str,
                        help='Config file for pretrained model.')
    parser.add_argument('--checkpoint_path', default=None, type=str, help='path for checkpoint file')
    parser.add_argument('-i','--input', required=True, type=str, help='input file')
    parser.add_argument('--output_filename', default='data/result/dcrnn_predictions.json')
    args = parser.parse_args()
    run_dcrnn(args)
